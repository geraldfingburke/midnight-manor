using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Generator : MonoBehaviour
{

	public Slider slider;
	public Image image;
	public Text text;
	public Text playAgainButton;
	public Text winText;
	public Image panel;
	public AudioSource sound;
	public AudioSource gameSound;
	public AudioSource gameOver;
	private bool isPlaying = false;

	public float timeMultiplier;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		slider.value -= Time.deltaTime*0.01f*timeMultiplier;
		if (slider.value <= 0)
		{
			if (!isPlaying)
			{
				gameSound.Stop();
				gameOver.Play();
				isPlaying = true;
			}
			panel.color = new Color(0,0,0,1);
			winText.text = "Oh, God, no...";
			winText.color = new Color(1,1,1,1);
			playAgainButton.color = new Color(1,1,1,1);
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		image.color = new Color(1, 1, 1, 1);
		text.text = null;
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			sound.Play();
			text.text = "I've restarted it";
			slider.value = 1;
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		image.color = new Color(1, 1, 1, 0);
		text.text = null;
	}
}