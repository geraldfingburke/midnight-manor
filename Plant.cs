using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plant : MonoBehaviour {

    public Image image;
    public Text text;

    void OnTriggerEnter2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 1);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 0);
        text.text = null;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (Input.GetKey(KeyCode.Space) && PlayerController.hasHook && PlayerController.hasLockBoxKey)
        {
            text.text = "There's nothing else down there...";
        }else if (Input.GetKeyDown(KeyCode.Space) && PlayerController.hasHook)
        {
            text.text = "Whoo! I got it! It's a key!";
            PlayerController.hasLockBoxKey = true;
        }else if (Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "I can see something shining in the bottom there, but I can't get my hand in the vase";
        }
    }
}