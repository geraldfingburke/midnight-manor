using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrontDoor : MonoBehaviour {

    public Image image;
    public Text text;
    public Text winText;
    public Text playAgainButton;
    public Image panel;
    public Text credits;

    public AudioSource victory;
    public AudioSource doorOpen;
    public AudioSource gameMusic;

    private Generator generator;
    private bool isPlaying;

    void Start()
    {
        generator = FindObjectOfType<Generator>();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 1);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 0);
        text.text = null;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (Input.GetKeyDown(KeyCode.Space) && PlayerController.hasHouseKey)
        {
            if (!isPlaying)
            {
                gameMusic.Stop();
                doorOpen.Play();
                victory.Play();
                isPlaying = true;
            }
            winText.text = "Freedom!";
            credits.color = new Color(0,0,0,1);
            winText.color = new Color(0,0,0,1);
            playAgainButton.color = new Color(0,0,0,1);
            panel.color = new Color(1,1,1,1);
            generator.slider.value = 1;
            
        }else if (Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "This seems to be the front door";
        }
    }
}