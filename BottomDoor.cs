using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomDoor : MonoBehaviour
{

	public Image upArrow;
	public Image downArrow;

	public float upTravel;
	public float downTravel;

	public bool canUp;
	public bool canDown;

	public AudioSource sound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (canUp)
		{
			upArrow.color = new Color(1, 1, 1, 1);
		}
		if (canDown)
		{
			downArrow.color = new Color(1,1,1,1);
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		upArrow.color = new Color(1, 1, 1, 0);
		downArrow.color = new Color(1, 1, 1, 0);

	}

	void OnTriggerStay2D(Collider2D col)
	{
		if (canUp && Input.GetKeyDown(KeyCode.UpArrow))
		{
			sound.Play();
			col.transform.position += new Vector3(0,upTravel);
		}
		if (canDown && Input.GetKeyDown(KeyCode.DownArrow))
		{
			sound.Play();
			col.transform.position -= new Vector3(0,downTravel);
		}
	}
}
