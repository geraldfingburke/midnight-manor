using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Television : MonoBehaviour {

    public Image image;
    public Text text;

    void OnTriggerEnter2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 1);
        text.text = null;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (PlayerController.hasBook && PlayerController.hasCrowbar && Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "There's nothing else behind here...";
        }
        else if (PlayerController.hasBook && Input.GetKeyDown(KeyCode.Space))
        {
            PlayerController.hasCrowbar = true;
            text.text = "It says to just flip this switch here aaand... A crowbar?";

        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "It looks like there's some kind of mechanism that keeps this thing on the wall. I have no idea how it works.";
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 0);
        text.text = null;
    }
}
