using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Workstation : MonoBehaviour
{

	public Image image;
	public Text text;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		image.color = new Color(1,1,1,1);
	    text.text = null;
	}

	void OnTriggerStay2D(Collider2D col)
	{
	    if (PlayerController.hasCrowbar && PlayerController.hasHook && Input.GetKeyDown(KeyCode.Space))
	    {
	        text.text = "I already have the hook";
	    }else if (PlayerController.hasCrowbar && Input.GetKeyDown(KeyCode.Space))
		{
			PlayerController.hasHook = true;
			text.text = "A hook on the end of a pole?";

		} else if (Input.GetKeyDown(KeyCode.Space))
		{
			text.text = "The drawer seems jammed. If only I had something to pry it open...";
		}
	}

	void OnTriggerExit2D(Collider2D col)
	{
		image.color = new Color(1,1,1,0);
		text.text = null;
	}
}
