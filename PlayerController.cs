using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

	public Camera camera;
	public float playerMoveSpeed;


	public static bool hasCodex = false;
	public static bool hasBook = false;
	public static bool hasCrowbar = false;
	public static bool hasHook = false;
	public static bool hasLockBoxKey = false;
	public static bool hasHouseKey = false;

	private Animator anim;

	// Use this for initialization
	void Start () {
		anim = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		camera.transform.position = this.transform.position - new Vector3(0,-0.6f,10);

		if (Input.GetAxis("Horizontal") >= 0.05f && transform.position.x <= 24)
		{
			anim.SetBool("isWalking", true);
			this.transform.position += Vector3.right*playerMoveSpeed;
			transform.localScale = new Vector3(1,1,1);


        }
        if (Input.GetAxis("Horizontal") <= -0.05f && transform.position.x >= -24)
		{
			anim.SetBool("isWalking", true);
			this.transform.position += Vector3.left*playerMoveSpeed;
			transform.localScale = new Vector3(-1,1,1);

        }
        if (Input.GetAxis("Horizontal") < 0.04f && Input.GetAxis("Horizontal") >= -0.4f)
		{
			anim.SetBool("isWalking", false);
		}
	}
}