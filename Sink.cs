using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sink : MonoBehaviour {

    public Image image;
    public Text text;

    void OnTriggerEnter2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 1);
    }

    void OnTriggerExit2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 0);
        text.text = null;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (Input.GetKeyDown(KeyCode.Space) && PlayerController.hasCodex)
        {
            text.text = "There's nothing left here...";
        }else if (Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "This seems to be some kind of codex";
            PlayerController.hasCodex = true;
        }
    }
}