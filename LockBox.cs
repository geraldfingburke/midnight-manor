using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockBox : MonoBehaviour
{

	public Image image;
	public Text text;

	void OnTriggerEnter2D(Collider2D col)
	{
		image.color = new Color(1, 1, 1, 1);
	}

	void OnTriggerExit2D(Collider2D col)
	{
		image.color = new Color(1, 1, 1, 0);
	    text.text = null;
	}

	void OnTriggerStay2D(Collider2D col)
	{
	    if (Input.GetKeyDown(KeyCode.Space) && PlayerController.hasLockBoxKey && PlayerController.hasHouseKey)
	    {
	        text.text = "I don't need any of the other keys";
	    }else if (Input.GetKeyDown(KeyCode.Space) && PlayerController.hasLockBoxKey)
		{
			text.text = "This must be the key to the door!";
			PlayerController.hasHouseKey = true;
		}else if (Input.GetKeyDown(KeyCode.Space))
		{
			text.text = "Hmm... That lock looks pretty tough";
		}
	}
}