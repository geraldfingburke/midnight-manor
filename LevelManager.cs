using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    public void LoadLevel(string name)
    {
        /*This is where I reset everything when the level is loaded
        rather than in a separate reset function*/
        PlayerController.hasLockBoxKey = false;
        PlayerController.hasBook = false;
        PlayerController.hasCodex = false;
        PlayerController.hasCrowbar = false;
        PlayerController.hasHook = false;
        PlayerController.hasHouseKey = false;
        SceneManager.LoadScene(name);
        
    }
    /* These are never actually used. This script is my one-size-fits-all solution
    for scene management in Unity, I use it everywhere and rarely change it*/
    public void LoadNextLevel()
    {
        int i = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(i + 1);
    }

    public void LoadPreviousLevel()
    {
        int i = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(i - 1);
    }

    public void Quit()
    {
        Application.Quit();
    }
}