using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bookshelf : MonoBehaviour {


    public Image image;
    public Text text;

    void OnTriggerEnter2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 1);
        text.text = null;
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (PlayerController.hasBook && PlayerController.hasCodex && Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "I don't think any of the other books will help";
        }
        else if (PlayerController.hasCodex && Input.GetKeyDown(KeyCode.Space))
        {
            PlayerController.hasBook = true;
            text.text = "'Welcome To Your Television' seems to be an owner's manual for a television";

        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            text.text = "These all seem to be in some weird language. There's one with a picture of a tv on it, but I can't understand any of the words...";
        }
    }

    void OnTriggerExit2D(Collider2D col)
    {
        image.color = new Color(1, 1, 1, 0);
        text.text = null;
    }
}
